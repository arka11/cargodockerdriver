package com.cargodocker.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cargodocker.driver.Utility.FileUtil;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class MainRegisterActivity extends AppCompatActivity {

    String resultText;
    static final int CAMERA_CAPTURE = 1;
    static final int DETECT_VP = 2;
    private Uri vpUri;
    File vp;
    ImageView imgCaturePic;
    int back_counter = 0;

    int counter = 0;

    TextView tvVp;
    Bitmap vpB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_register);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        init();

    }

    private void init() {

        tvVp = findViewById(R.id.tv_vp);
        imgCaturePic = findViewById(R.id.iv_dl);

        findViewById(R.id.tv_dvl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainRegisterActivity.this)
                        .setTitle("Please note!")
                        .setMessage("Take images in landscape mode")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                counter = 0;
                                findViewById(R.id.ll_vp).setVisibility(View.GONE);
                                vp = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                vpUri = Uri.fromFile(vp); // convert path to Uri
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, vpUri);
                                startActivityForResult(takePictureIntent, CAMERA_CAPTURE);
                            }
                        })
                        .show();


            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                Uri uri = vpUri;
                try {
                    vpB = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    imgCaturePic.setImageBitmap(vpB);
                    imgCaturePic.setVisibility(View.VISIBLE);
                    readPlate(vpB);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }


    }

    private void readPlate(Bitmap b) {
        final Bitmap bitmap = b;
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(b);
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();

        FirebaseVisionCloudTextRecognizerOptions options = new FirebaseVisionCloudTextRecognizerOptions.Builder()
                .setLanguageHints(Arrays.asList("en", "hi"))
                .build();
        Task<FirebaseVisionText> result =
                detector.processImage(image)
                        .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                            @Override
                            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                 resultText = firebaseVisionText.getText();

                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // Task failed with an exception
                                        // ...
                                    }
                                });
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void regDetails(View view) {

        if (imgCaturePic.getVisibility() == View.VISIBLE) {
            Intent intent = new Intent(MainRegisterActivity.this, OCRActivity.class);
            intent.putExtra("ocr",resultText);
            startActivity(intent);
        } else {
            Toast.makeText(MainRegisterActivity.this, "PLease enter all the details!", Toast.LENGTH_LONG).show();
        }
    }


    private boolean isValidPlate(String plate) {

        String EMAIL_STRING = "^[A-Z]{2}[ -]{0,1}[0-9]{2}[ -]{0,1}[A-Z]{1,2}[ -]{0,1}[0-9]{1,4}$";
        if (Pattern.compile(EMAIL_STRING).matcher(plate).matches()){
        }else {
            Log.d("tag","plate "+plate);

        }

        return Pattern.compile(EMAIL_STRING).matcher(plate).matches();

    }

    @Override
    public void onBackPressed() {
        if (back_counter > 0) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, "Press back again to exit", Toast.LENGTH_LONG).show();
            back_counter++;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    back_counter = 0;
                }
            }, 5000);
        }
    }
}

