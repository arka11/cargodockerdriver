package com.cargodocker.driver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class OtpActivity extends AppCompatActivity {

    TextView tvNum;
    EditText[] etArray = new EditText[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        init();

    }

    private void init() {
        tvNum = findViewById(R.id.tv_num);
        tvNum.setText("An otp has been sent to "+getIntent().getStringExtra("num")+".");
        etArray[0] = findViewById(R.id.et1);
        etArray[1] = findViewById(R.id.et2);
        etArray[2] = findViewById(R.id.et3);
        etArray[3] = findViewById(R.id.et4);

        for (int i=0;i<etArray.length;i++){
            final int finalI = i;
            etArray[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (etArray[finalI].getText().toString().length()>0){
                        if (finalI!= (etArray.length-1)){
                            etArray[finalI+1].requestFocus();
                        }
                    }
                }
            });
        }
    }

    public void verifyOtp(View view) {
        String otp = "";
        for (int i=0;i<etArray.length;i++){
            otp=otp+etArray[i].getText().toString();
        }
        if (otp.length()==4){
            Intent intent = new Intent(OtpActivity.this,MainRegisterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this,"Please enter the otp!",Toast.LENGTH_LONG).show();
        }
    }
}
