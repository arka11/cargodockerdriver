package com.cargodocker.driver.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cargodocker.driver.DashBoard;
import com.cargodocker.driver.DetailsActivity;
import com.cargodocker.driver.QRActivity;
import com.cargodocker.driver.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActiveBooking extends Fragment {

    View v;
    TextView tvBookingNum;

    public ActiveBooking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        DashBoard activity = ((DashBoard)getActivity());
        activity.searchView.setVisibility(View.GONE);
        v = inflater.inflate(R.layout.fragment_active_booking, container, false);
        init();
        return v;
    }

    private void init() {
        tvBookingNum = v.findViewById(R.id.tv_booking_num);
        v.findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("num","");
                startActivity(intent);
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }
}
