package com.cargodocker.driver.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SearchView;
import android.widget.TextView;

import com.cargodocker.driver.DashBoard;
import com.cargodocker.driver.DetailsActivity;
import com.cargodocker.driver.Object.BookingObj;
import com.cargodocker.driver.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PastBooking extends Fragment {

    RecyclerView rvBooking;
    List<BookingObj> listBookings = new ArrayList<>();
    BookingAdapter bookingAdapter;
    View v;

    public PastBooking() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_past_booking, container, false);
        init();
        return v;

    }

    private void init() {
        DashBoard activity = ((DashBoard)getActivity());
        activity.searchView.setVisibility(View.VISIBLE);
        activity.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                bookingAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                bookingAdapter.getFilter().filter(s);
                return false;
            }
        });
        listBookings.add(new BookingObj("Booking101","HYD","BLR","DL01AE8010","12/03/20"));
        listBookings.add(new BookingObj("Booking102","HYD","BOM","MH01AE8037","12/03/20"));
        listBookings.add(new BookingObj("Booking103","BLR","DEL","TS01AE8014","12/03/20"));
        listBookings.add(new BookingObj("Booking104","HYD","NGP","AP01AE8018","13/03/20"));
        listBookings.add(new BookingObj("Booking105","HYD","BLR","TS01AE8017","14/03/20"));
        listBookings.add(new BookingObj("Booking106","BOM","HYD","AP01AE8013","16/03/20"));
        listBookings.add(new BookingObj("Booking107","BLR","HYD","AP01AE8015","16/03/20"));
        rvBooking = v.findViewById(R.id.rv_bookings);
        rvBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        bookingAdapter = new BookingAdapter(listBookings);
        rvBooking.setAdapter(bookingAdapter);
    }



    class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> implements Filterable {

        List<BookingObj> list_filtered;

        BookingAdapter(List<BookingObj> list_filtered){
            this.list_filtered = list_filtered;
        }

        @NonNull
        @Override
        public BookingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.layout_active_bookings,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull BookingAdapter.ViewHolder holder, final int position) {
            holder.tvBookingNum.setText(list_filtered.get(position).getBookingNum());
            holder.tvDate.setText(list_filtered.get(position).getDate());
            String text = list_filtered.get(position).getShipFrom()+" &#10132 "+list_filtered.get(position).getShipTo();
            holder.tvLoc.setText(Html.fromHtml(text));
            holder.tvVehicleNum.setText(list_filtered.get(position).getVehicleNum());

        }

        @Override
        public int getItemCount() {
            return list_filtered.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }



        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String string = constraint.toString();
                    Log.v("TAG", string);
                    if (string.isEmpty()) {
                        list_filtered = listBookings;
                    } else {
                        List<BookingObj> filteredList = new ArrayList<>();
                        for (BookingObj s : listBookings) {
                            if (s.getBookingNum().toLowerCase().contains(string.toLowerCase()) || s.getBookingNum().toLowerCase().contains(string.toLowerCase())) {
                                Log.v("TAG", s.getBookingNum() + " " + string);
                                filteredList.add(s);
                            }
                        }
                        list_filtered = filteredList;
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = list_filtered;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    list_filtered = (List<BookingObj>) results.values;
                    notifyDataSetChanged();
                }
            };
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvBookingNum,tvDate,tvLoc,tvVehicleNum;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                tvBookingNum = itemView.findViewById(R.id.tv_booking_num);
                tvDate = itemView.findViewById(R.id.tv_date);
                tvLoc = itemView.findViewById(R.id.tv_loc);
                tvVehicleNum = itemView.findViewById(R.id.tv_vehicle_num);
            }
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }
}
