package com.cargodocker.driver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.cargodocker.driver.Utility.AppUrls;

public class DetailsActivity extends AppCompatActivity {

    TextView tvQr;
    TextView tvBookingNum;
    SharedPreferences sh_Pref;
    CheckBox cbDriver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();




    }

    @Override
    protected void onResume() {
        if (!sh_Pref.getString("qr_code","").equalsIgnoreCase("")){
            findViewById(R.id.tv_qr).setVisibility(View.GONE);
            findViewById(R.id.tv_qr_value).setVisibility(View.GONE);
            findViewById(R.id.cb_driver).setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    cbDriver.setChecked(true);
                }
            },1000);
            tvQr.setText(sh_Pref.getString("qr_code",""));
//            tvQr.setVisibility(View.VISIBLE);
        }
        super.onResume();
    }

    private void init() {
        sh_Pref = getSharedPreferences(AppUrls.shCredentials, MODE_PRIVATE);

        tvQr = findViewById(R.id.tv_qr_value);
        cbDriver = findViewById(R.id.cb_driver);
        tvBookingNum = findViewById(R.id.tv_booking_num);
        tvBookingNum.setText(getIntent().getStringExtra("num"));
        findViewById(R.id.tv_qr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailsActivity.this,QRActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {

        if (sh_Pref.getString("qr_code","").equalsIgnoreCase("")){
            super.onBackPressed();
        }else {
            Toast.makeText(this,"Not Permitted!",Toast.LENGTH_SHORT).show();
        }
    }
}
