package com.cargodocker.driver.Object;

public class BookingObj {

    String bookingNum,shipFrom,shipTo,vehicleNum,date;
    public BookingObj(String bookingNum, String shipFrom, String shipTo, String vehicleNum, String date){
        this.bookingNum = bookingNum;
        this.shipFrom = shipFrom;
        this.shipTo = shipTo;
        this.vehicleNum = vehicleNum;
        this.date = date;
    }

    public String getBookingNum() {
        return bookingNum;
    }

    public String getDate() {
        return date;
    }

    public String getShipFrom() {
        return shipFrom;
    }

    public String getShipTo() {
        return shipTo;
    }

    public String getVehicleNum() {
        return vehicleNum;
    }
}
