package com.cargodocker.driver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class RegisterActivity extends AppCompatActivity {

    TextInputEditText etPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
    }

    private void init() {
        etPhone = findViewById(R.id.et_ph_num);
    }

    public void registerNum(View view) {
        String num = etPhone.getText().toString();
        if (num.length()==0 || num.length()!= 10){
            Toast.makeText(this,"PLease enter a correct phone number!",Toast.LENGTH_LONG).show();
        }else{
            Intent intent = new Intent(RegisterActivity.this,OtpActivity.class);
            intent.putExtra("num",num);
            startActivity(intent);
        }
    }


}
